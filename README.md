# README #

Build your own vocabulary list and learn them later when you want.

## Description

Pocket Vocabulary is a simple app that lets you write new foreign words to your vocabulary. Whenever you come across a new word, just simply save it into your vocabulary and learn it later when you want.

Pocket Vocabulary has flash cards to help you learn your new words.

Pocket Vocabulary has a feature that let you look up your new word online that makes you easier to add new words.

## App features ##
- Create your own vocabulary list of your own
- Flash cards to help you memorize your vocabulary.
- Dictionary. You can look up your vocabulary on online dictionary. Currently supports multilingual online dictionary (www.glosbe.com)

## Google Play ##

[Pocket Vocabulary](https://play.google.com/store/apps/details?id=com.gmail.tararenko.pocketvocabulary)