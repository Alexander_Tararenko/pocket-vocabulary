package com.gmail.tararenko.pocketvocabulary.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder> {

    private final LayoutInflater mLayoutInflater;
    private List<WordModel> words;
    private OnItemClickListener mListener;
    private Context mContext;
    public WordListAdapter(Context context, List<WordModel> words) {
        validateWords(words);
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.words = words;
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.mLayoutInflater.inflate(R.layout.word_row, parent, false);
        WordViewHolder viewHolder = new WordViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WordViewHolder wordViewHolder, final int position) {
        final WordModel word = words.get(position);
        wordViewHolder.mWord.setText(word.getWord());
        wordViewHolder.mType.setText(getWordTypeAsString(word.getWordType()));
        wordViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (WordListAdapter.this.mListener != null) {
                    WordListAdapter.this.mListener.onWordItemClicked(word.getWordId());
                }
            }
        });
    }

    public void sort(Comparator<WordModel> comparator) {
        if (words != null) {
            Collections.sort(words, comparator);
            this.notifyDataSetChanged();
        }
    }

    public void setWords(List<WordModel> words) {
        validateWords(words);
        this.words = words;
        this.notifyDataSetChanged();
    }

    public void validateWords(List<WordModel> words) {
        if (words == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    public void setListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return words == null ? 0 : words.size();
    }

    @Override
    public long getItemId(int position) {
        return words.get(position).getWordId();
    }

    private String getWordTypeAsString(WordModel.WordType type) {
        String[] types = mContext.getResources().getStringArray(R.array.parts_of_speech);
        switch (type) {
            case NOUN:
                return types[0];
            case VERB:
                return types[1];
            case IDIOM:
                return types[2];
            case PARTICIPLE:
                return types[3];
            case ADJECTIVE:
                return types[4];
            case ADVERB:
                return types[5];
            case PHRASAL_VERB:
                return types[6];
            case PREPOSITION:
                return types[7];
        }
        return "";
    }

    public interface OnItemClickListener {
        void onWordItemClicked(long wordId);
    }

    static class WordViewHolder extends RecyclerView.ViewHolder {

        public TextView mWord;
        public TextView mType;

        public WordViewHolder(View itemView) {
            super(itemView);
            this.mWord = (TextView) itemView.findViewById(R.id.word);
            this.mType = (TextView) itemView.findViewById(R.id.word_type);
        }
    }

}
