package com.gmail.tararenko.pocketvocabulary.domain.backup.parser;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import org.xmlpull.v1.XmlPullParserException;

import java.io.FileNotFoundException;
import java.util.List;

public interface FeedParser {

    String TAG_WORDS = "words";
    String TAG_ENTRY = "entry";
    String TAG_WORD = "word";
    String TAG_TYPE = "type";
    String TAG_TRANSLATION = "translation";
    String TAG_DEFINITION = "definition";
    String TAG_EXAMPLE = "example";
    String TAG_EXAMPLE_TRANSLATION = "example_translation";
    String TAG_SYNONYM = "synonym";
    String ATTRIBUTE_NUMBER = "number";
    String ATTRIBUTE_ID = "id";

    List<WordModel> parse() throws XmlPullParserException, FileNotFoundException;

}
