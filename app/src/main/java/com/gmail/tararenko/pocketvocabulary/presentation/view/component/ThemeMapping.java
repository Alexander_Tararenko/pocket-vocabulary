package com.gmail.tararenko.pocketvocabulary.presentation.view.component;

public interface ThemeMapping {

    String THEME_NAME_KEY = "theme";
    String THEME_COLOR_KEY = "theme color";

    String BLUEGRAY_THEME = "bluegray";
    String RED_THEME = "red";

}
