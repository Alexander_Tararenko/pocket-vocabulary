package com.gmail.tararenko.pocketvocabulary.domain.util;

public class Random {

    private java.util.Random random;

    public Random() {
        random = new java.util.Random();
    }

    public Random(int seed) {
        random = new java.util.Random(seed);
    }

    /**
     * Returns a pseudo-random (approximately) exponential distributed int in the [0, bound)
     * Expected value = bound / 2
    */
    public int nextExpInt(int bound) {
        if (bound < 0) {
            return 0;
        }
        int lambda = bound / 2;
        return (int) (-(1.0 / lambda) * Math.log(this.random.nextDouble()) * lambda * bound) % bound;
    }

}
