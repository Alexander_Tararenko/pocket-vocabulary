package com.gmail.tararenko.pocketvocabulary.domain.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Class for language conversion from normal name to iso3
 */
public class Languages {

    private static Map<String, Locale> locales = new HashMap<>();
    static {
        for (String l: Locale.getISOLanguages()) {
            Locale locale = new Locale(l);
            locales.put(locale.getDisplayLanguage(new Locale("en")), locale);
        }
    }

    public static Locale getLocale(String name) {
        return locales.get(name);
    }

    public static String[] getLanguages() {
        return locales.keySet().toArray(new String[locales.size()]);
    }

}
