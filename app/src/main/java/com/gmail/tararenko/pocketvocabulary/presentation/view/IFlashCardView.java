package com.gmail.tararenko.pocketvocabulary.presentation.view;

import android.content.Context;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

public interface IFlashCardView {

    void onBack();

    Context getContext();

    void setWord(WordModel word);

    boolean isFront();

    void showNext(WordModel word);

    void showBack();

    void showEnd();

}
