package com.gmail.tararenko.pocketvocabulary;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

public class MyApplication extends Application {

    private static final int LAUNCHES_FOR_RATE = 3;

    @Override
    public void onCreate() {
        super.onCreate();
        initDefaultPreferences();
        checkRate();
    }

    private void initDefaultPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String from = preferences.getString(getString(R.string.preferences_translation_from_key), "");
        if (from.equals("")) {
            preferences.edit().putString(getString(R.string.preferences_translation_from_key), "English").apply();
        }
        String to = preferences.getString(getString(R.string.preferences_translation_to_key), "");
        if (to.equals("")) {
            preferences.edit().putString(getString(R.string.preferences_translation_to_key), "Spanish").apply();
        }
    }

    private void checkRate() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isRated = preferences.getBoolean("rated", false);
        if (!isRated) {
            int launches = preferences.getInt("launches", 0);
            launches++;
            preferences.edit().putInt("launches", launches).apply();
            if (launches > LAUNCHES_FOR_RATE) {
                preferences.edit().putBoolean("showRateDialog", true).apply();
            }
        }
    }

}
