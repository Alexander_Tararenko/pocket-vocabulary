package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import android.content.Context;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.data.database.DatabaseManager;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IWordListView;

import java.util.List;

public class WordListPresenter implements IWordListPresenter {

    private IWordListView mView;

    public WordListPresenter(IWordListView view) {
        this.mView = view;
    }

    @Override
    public List<WordModel> getWords() {
        Context context = mView.getContext();
        DatabaseManager manager = new DatabaseManager(context);
        List<WordModel> result = manager.getWords();
        manager.closeConnection();
        return result;
    }

    @Override
    public List<WordModel> getWords(List<WordModel.WordType> types) {
        List<WordModel> result;
        if (types == null || types.size() == 0) {
             result = getWords();
        } else {
            Context context = mView.getContext();
            DatabaseManager manager = new DatabaseManager(context);
            result = manager.getWords(types);
            manager.closeConnection();
        }
        return result;
    }

    @Override
    public void onWordClicked(long wordId) {
        this.mView.viewWord(wordId);
    }

    @Override
    public void onAddClick() {
        mView.addNewWord();
    }

    @Override
    public void onOptionsItemSelected(int itemId) {
        switch (itemId) {
            case R.id.action_sort:
                mView.showSortByDialog();
                break;
            case R.id.action_flash_card:
                mView.runFlashCard();
                break;
            case R.id.action_filter:
                mView.showFilterByDialog();
                break;
            case R.id.action_settings:
                mView.showSettings();
                break;
        }
    }
}
