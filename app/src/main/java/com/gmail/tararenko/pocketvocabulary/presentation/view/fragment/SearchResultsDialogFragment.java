package com.gmail.tararenko.pocketvocabulary.presentation.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.domain.web.glosbe.GlosbeProvider;
import com.gmail.tararenko.pocketvocabulary.presentation.model.SearchResultModel;
import com.gmail.tararenko.pocketvocabulary.presentation.view.adapter.SearchResultAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SearchResultsDialogFragment extends DialogFragment {

    private static String ARG_WORD = "word";
    private static String ARG_FROM = "from";
    private static String ARG_TO = "to";
    private SearchResultsDialogListener mCallback;
    private RecyclerView mListView;
    private SearchResultAdapter mAdapter;
    private TextView mEmptyTextView;
    private ProgressBar mSpinner;
    private List<SearchResultModel> mResults;
    private LoadResultsTask mTask;
    private String word;
    private String from;
    private String to;
    private boolean failedResults = false;

    public static SearchResultsDialogFragment getInstance(String word, String from, String to) {
        SearchResultsDialogFragment fragment = new SearchResultsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_WORD, word);
        bundle.putString(ARG_FROM, from);
        bundle.putString(ARG_TO, to);
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        initArguments(getArguments());
        View bodyView = getBodyView();
        mTask = new LoadResultsTask();
        mTask.execute(new Void[1]);
        return createAlertDialog(bodyView);
    }

    private View getBodyView() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View bodyView = inflater.inflate(R.layout.dialog_fragment_search_result,
                new FrameLayout(getContext()), false);
        this.mEmptyTextView = (TextView) bodyView.findViewById(R.id.empty_result);
        TextView powered = (TextView) bodyView.findViewById(R.id.powered_by_glosbe);
        powered.setText(Html.fromHtml(getString(R.string.powered_by) + " <a href=\"https://www.glosbe.com\">Glosbe</a>"));
        powered.setMovementMethod(LinkMovementMethod.getInstance());
        mListView = (RecyclerView) bodyView.findViewById(R.id.result_list);
        mListView.setLayoutManager(new LinearLayoutManager(getContext()));
        mSpinner = (ProgressBar) bodyView.findViewById(R.id.result_spinner);
        mSpinner.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.bluegrayPrimaryColor), PorterDuff.Mode.MULTIPLY);
        return bodyView;
    }

    public void renderResults(List<SearchResultModel> results) {
        setResults(results);
    }

    public void setCallback(SearchResultsDialogListener callback) {
        this.mCallback = callback;
    }

    private void setResults(List<SearchResultModel> results) {
        if (results != null) {
            if (mAdapter == null) {
                mAdapter = new SearchResultAdapter(getContext(), results);
            } else {
                mAdapter.setResults(results);
            }
            mListView.setAdapter(mAdapter);
            this.mResults = results;
            checkAdapterIsEmpty();
        }
    }

    private AlertDialog createAlertDialog(View bodyView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.search_in_web_dialog_title)
                .setView(bodyView)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mTask.cancel(false);
                        if (mCallback != null && mAdapter != null) {
                            mCallback.onConfirm(chooseResults(mAdapter.getChecked()));
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mTask.cancel(false);
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    private void initArguments(Bundle bundle) {
        this.word = bundle.getString(ARG_WORD);
        this.from = bundle.getString(ARG_FROM);
        this.to = bundle.getString(ARG_TO);
    }

    private void checkAdapterIsEmpty() {
        if (mAdapter.getItemCount() == 0) {
            setEmptyText();
            mEmptyTextView.setVisibility(View.VISIBLE);
        } else {
            mEmptyTextView.setVisibility(View.GONE);
        }
    }

    private void setEmptyText() {
        if (failedResults) {
            mEmptyTextView.setText(getString(R.string.error_result));
        } else {
            mEmptyTextView.setText(getString(R.string.empty_result));
        }
    }

    private List<SearchResultModel> chooseResults(boolean[] checked) {
        List<SearchResultModel> results = new LinkedList<>();
        for (int i = 0; i < mResults.size(); i++) {
            if (checked[i]) {
                results.add(mResults.get(i));
            }
        }
        return results;
    }

    private void showSpinner() {
        mSpinner.setVisibility(View.VISIBLE);
    }

    private void hideSpinner() {
        mSpinner.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    public interface SearchResultsDialogListener {
        void onConfirm(List<SearchResultModel> results);
    }

    public class LoadResultsTask extends AsyncTask<Void, Void, List<SearchResultModel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showSpinner();
        }

        @Override
        protected List<SearchResultModel> doInBackground(Void... params) {
            List<SearchResultModel> results = new ArrayList<>();
            GlosbeProvider provider = new GlosbeProvider(word, from, to);
            try {
                provider.parse();
                results = provider.getResults();
                failedResults = false;
            } catch (IOException e) {
                failedResults = true;
            }
            return results;
        }

        @Override
        protected void onPostExecute(List<SearchResultModel> searchResultModels) {
            hideSpinner();
            renderResults(searchResultModels);
            super.onPostExecute(searchResultModels);
        }
    }

}
