package com.gmail.tararenko.pocketvocabulary.presentation.model;

import java.io.Serializable;

public class WordModel implements Serializable {

    private final long wordId;
    private String word;
    private WordType wordType;
    private String definition;
    private String translation;
    private String example;
    private String exampleTranslation;
    private String synonym;
    public WordModel(long wordId) {
        this.wordId = wordId;
    }

    public long getWordId() {
        return wordId;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = trim(word);
    }

    public WordType getWordType() {
        return wordType;
    }

    public void setWordType(WordType wordType) {
        this.wordType = wordType;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = trim(definition);
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = trim(translation);
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = trim(example);
    }

    public String getExampleTranslation() {
        return exampleTranslation;
    }

    public void setExampleTranslation(String exampleTranslation) {
        this.exampleTranslation =trim(exampleTranslation);
    }

    public String getSynonym() {
        return synonym;
    }

    public void setSynonym(String synonym) {
        this.synonym = trim(synonym);
    }

    private String trim(String str) {
        if (str != null) {
            String result = str.trim();
            return result.isEmpty() ? null : result;
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordModel wordModel = (WordModel) o;

        if (getWord() != null ? !getWord().equals(wordModel.getWord()) : wordModel.getWord() != null)
            return false;
        if (getWordType() != wordModel.getWordType()) return false;
        if (getDefinition() != null ? !getDefinition().equals(wordModel.getDefinition()) : wordModel.getDefinition() != null)
            return false;
        if (getTranslation() != null ? !getTranslation().equals(wordModel.getTranslation()) : wordModel.getTranslation() != null)
            return false;
        if (getExample() != null ? !getExample().equals(wordModel.getExample()) : wordModel.getExample() != null)
            return false;
        if (getExampleTranslation() != null ? !getExampleTranslation().equals(wordModel.getExampleTranslation()) : wordModel.getExampleTranslation() != null)
            return false;
        return !(getSynonym() != null ? !getSynonym().equals(wordModel.getSynonym()) : wordModel.getSynonym() != null);

    }

    @Override
    public int hashCode() {
        int result = getWord() != null ? getWord().hashCode() : 0;
        result = 31 * result + (getWordType() != null ? getWordType().hashCode() : 0);
        result = 31 * result + (getDefinition() != null ? getDefinition().hashCode() : 0);
        result = 31 * result + (getTranslation() != null ? getTranslation().hashCode() : 0);
        result = 31 * result + (getExample() != null ? getExample().hashCode() : 0);
        result = 31 * result + (getExampleTranslation() != null ? getExampleTranslation().hashCode() : 0);
        result = 31 * result + (getSynonym() != null ? getSynonym().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WordModel{" +
                "wordId=" + wordId +
                ", word='" + word + '\'' +
                ", wordType=" + wordType.getFieldDescription() +
                ", definition='" + definition + '\'' +
                ", translation='" + translation + '\'' +
                ", example='" + example + '\'' +
                ", exampleTranslation='" + exampleTranslation + '\'' +
                ", synonym='" + synonym + '\'' +
                '}';
    }

    public enum WordType {
        NOUN("Noun"), VERB("Verb"), IDIOM("Idiom"), PARTICIPLE("Participle"), ADJECTIVE("Adjective"),
        ADVERB("Adverb"), PHRASAL_VERB("Phrasal verb"), PREPOSITION("Preposition");

        private final String fieldDescription;

        WordType(String value) {
            fieldDescription = value;
        }

        public static WordType getEnumFromString(String name) {
            String value = name.trim();
            for (WordType type : WordType.values()) {
                if (value.equalsIgnoreCase(type.getFieldDescription())) {
                    return type;
                }
            }
            throw new IllegalArgumentException("No Enum specified for this string");
        }

        public static WordType getEnumFromNumber(int position) {
            switch (position) {
                case 1:
                    return NOUN;
                case 2:
                    return VERB;
                case 3:
                    return IDIOM;
                case 4:
                    return PARTICIPLE;
                case 5:
                    return ADJECTIVE;
                case 6:
                    return ADVERB;
                case 7:
                    return PHRASAL_VERB;
                case 8:
                    return PREPOSITION;
                default:
                    return NOUN;
            }
        }

        public int getPosition() {
            switch (this) {
                case NOUN:
                    return 1;
                case VERB:
                    return 2;
                case IDIOM:
                    return 3;
                case PARTICIPLE:
                    return 4;
                case ADJECTIVE:
                    return 5;
                case ADVERB:
                    return 6;
                case PHRASAL_VERB:
                    return 7;
                case PREPOSITION:
                    return 8;
                default:
                    return 0;
            }
        }

        public String getFieldDescription() {
            return fieldDescription;
        }

    }
}
