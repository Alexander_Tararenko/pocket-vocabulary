package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.data.database.DatabaseManager;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IWordEditView;

public class WordEditPresenter implements IWordEditPresenter {

    private IWordEditView mView;

    public WordEditPresenter(IWordEditView mView) {
        this.mView = mView;
    }

    @Override
    public void onOptionsItemSelected(int itemId) {
        if (itemId == R.id.action_confirm) {
            if (mView.validateData()) {
                mView.onConfirmClicked(replaceWord(mView.getData()));
            } else {
                mView.setErrorEmptyMessages();
            }
        }
        if (itemId == android.R.id.home) {
            mView.onBack();
        }
    }

    @Override
    public WordModel getWord(long wordId) {
        DatabaseManager manager = new DatabaseManager(mView.getContext());
        WordModel result = manager.getWord(wordId);
        manager.closeConnection();
        return result;
    }

    @Override
    public void findWord() {
        if (!checkInternetConnection()) {
            mView.showConnectionErrorDialog();
            return;
        }
        mView.showSearchResultDialog();
    }

    private boolean checkInternetConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mView.getContext()
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info != null) {
            if (info.isConnected() && info.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    private long replaceWord(WordModel word) {
        DatabaseManager manager = new DatabaseManager(mView.getContext());
        long id = manager.replaceWord(word);
        manager.closeConnection();
        return id;
    }

}
