package com.gmail.tararenko.pocketvocabulary.domain.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class HttpRetriever {

    public static String retrieve(String url) throws IOException {
        StringBuilder builder = new StringBuilder();
        URLConnection connection = (new URL(url)).openConnection();
        connection.setConnectTimeout(15 * 1000); // 15 sec
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }
        reader.close();
        return builder.toString();
    }

}
