package com.gmail.tararenko.pocketvocabulary.presentation.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.FlashCardPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.IFlashCardPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IFlashCardView;
import com.gmail.tararenko.pocketvocabulary.presentation.view.component.FlashCardLayout;

public class FlashCardFragment extends Fragment implements IFlashCardView {

    private OnFragmentInteractionListener mCallback;
    private FlashCardLayout mFlashCardFront;
    private FlashCardLayout mFlashCardBack;
    private FrameLayout mEndText;
    private IFlashCardPresenter mPresenter;
    private WordModel mWord;
    private TextView mFrontText;
    private TextView mBackText;
    private Animation flipIn;
    private Animation flipOut;
    private Animation slideOut;
    private Animation slideIn;
    private boolean isAnimationStarted;
    private boolean isRepeat;
    private String frontField;
    private String backField;

    public FlashCardFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_flash_card, container, false);
        this.mEndText = (FrameLayout) rootView.findViewById(R.id.end_text);
        this.mEndText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.refreshWords();
            }
        });
        this.mFrontText = (TextView) rootView.findViewById(R.id.flash_card_front_text);
        this.mBackText = (TextView) rootView.findViewById(R.id.flash_card_back_text);
        this.mFlashCardFront = (FlashCardLayout) rootView.findViewById(R.id.flash_card_front);
        this.mFlashCardBack = (FlashCardLayout) rootView.findViewById(R.id.flash_card_back);
        View.OnClickListener flashCardListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAnimationStarted) {
                    mPresenter.toggle();
                }
            }
        };
        this.mFlashCardBack.setOnClickListener(flashCardListener);
        this.mFlashCardFront.setOnClickListener(flashCardListener);
        initAnimation();
        setHasOptionsMenu(true);
        this.isAnimationStarted = false;
        this.isRepeat = false;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        frontField = preferences.getString(getString(R.string.preferences_front_key), "Noun");
        backField = preferences.getString(getString(R.string.preferences_back_key), "Translation");
        return rootView;
    }

    @Override
    public void setWord(WordModel word) {
        if (word != null) {
            this.mWord = word;
        }
        setFrontText();
        setBackText();
    }

    private void setFrontText() {
        try {
            this.mFrontText.setText(getWordFieldByName(frontField));
        } catch (IllegalArgumentException e) {
            this.mFrontText.setText(mWord.getWord());
        }
    }

    private void setBackText() {
        try {
            this.mBackText.setText(getWordFieldByName(backField));
        } catch (IllegalArgumentException e) {
            this.mBackText.setText(mWord.getTranslation());
        }
    }

    @Override
    public boolean isFront() {
        return mFlashCardFront.getVisibility() == View.VISIBLE;
    }

    @Override
    public void showNext(WordModel word) {
        mPresenter.incCounter(mWord);
        hideEndText();
        this.mWord = word;
        setFrontText();
        this.mFlashCardFront.setVisibility(View.VISIBLE);
        if (isRepeat) {
            isRepeat = false;
        } else {
            this.mFlashCardBack.startAnimation(slideOut);
        }
        this.mFlashCardFront.startAnimation(slideIn);
    }

    @Override
    public void showBack() {
        this.mFlashCardFront.startAnimation(flipOut);
    }

    @Override
    public void showEnd() {
        this.mEndText.setVisibility(View.VISIBLE);
        this.mFlashCardBack.startAnimation(slideOut);
        isRepeat = true;
    }

    private void hideEndText() {
        if (this.mEndText.getVisibility() == View.VISIBLE) {
            this.mEndText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (this.mPresenter == null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            try {
                Integer quantity = Integer.parseInt(preferences.getString(
                        getString(R.string.preferences_quantity_key), "10"));
                this.mPresenter = new FlashCardPresenter(this, quantity);
            } catch (NumberFormatException e) {
                this.mPresenter = new FlashCardPresenter(this, 0);
            }

        }
        setWord(this.mPresenter.getCurrentWord());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_flash_card);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        mPresenter.onOptionsItemSelected(id);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Context getContext() {
        return this.getActivity();
    }

    @Override
    public void onBack() {
        getActivity().onBackPressed();
    }

    private void initAnimation() {
        flipIn = AnimationUtils.loadAnimation(getContext(), R.anim.card_flip_in);
        flipOut = AnimationUtils.loadAnimation(getContext(), R.anim.card_flip_out);
        slideOut = AnimationUtils.loadAnimation(getContext(), R.anim.card_slide_out);
        slideIn = AnimationUtils.loadAnimation(getContext(), R.anim.card_slide_in);
        flipOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mFlashCardBack.setVisibility(View.INVISIBLE);
                isAnimationStarted = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFlashCardFront.setVisibility(View.INVISIBLE);
                mFlashCardBack.setVisibility(View.VISIBLE);
                mFlashCardBack.startAnimation(flipIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        slideOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isAnimationStarted = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimationStarted = false;
                mFlashCardBack.setVisibility(View.INVISIBLE);
                setBackText();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        Animation.AnimationListener defaultListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isAnimationStarted = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimationStarted = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        slideIn.setAnimationListener(defaultListener);
        flipIn.setAnimationListener(defaultListener);
    }

    private String getWordFieldByName(String field) throws IllegalArgumentException {
        field = field.trim().toLowerCase().replace("", "");
        if (field.contains("word")) {
            return mWord.getWord();
        } else if (field.contains("definition")) {
            return mWord.getDefinition();
        } else if (field.contains("example") && field.contains("translation")) {
            return mWord.getExampleTranslation();
        } else if (field.contains("example")) {
            return mWord.getExample();
        } else if (field.contains("translation")) {
            return mWord.getTranslation();
        } else if (field.contains("synonym")) {
            return mWord.getSynonym();
        }
        throw new IllegalArgumentException();
    }

    public interface OnFragmentInteractionListener {

    }

}
