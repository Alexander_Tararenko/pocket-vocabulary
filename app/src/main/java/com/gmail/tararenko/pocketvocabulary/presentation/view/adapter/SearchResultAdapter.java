package com.gmail.tararenko.pocketvocabulary.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.presentation.model.SearchResultModel;

import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ResultViewHolder> {

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private List<SearchResultModel> results;
    private boolean[] checked;

    public SearchResultAdapter(Context context, List<SearchResultModel> results) {
        validateResults(results);
        this.mContext = context;
        setResults(results);
        this.mLayoutInflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.mLayoutInflater.inflate(R.layout.search_result_row, parent, false);
        return new ResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResultViewHolder holder, final int position) {
        final SearchResultModel result = results.get(position);
        setText(holder.mTextDefinition, result.definition);
        setText(holder.mTextTranslation, result.translation);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checked[position] = isChecked;
            }
        });
    }


    @Override
    public int getItemCount() {
        return results.size();
    }

    public boolean[] getChecked() {
        return checked;
    }

    public void setResults(List<SearchResultModel> results) {
        validateResults(results);
        checked = new boolean[results.size()];
        this.results = results;
        this.notifyDataSetChanged();
    }

    public void validateResults(List<SearchResultModel> results) {
        if (results == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    private void setText(TextView view, String text) {
        if (text != null && !text.trim().isEmpty()) {
            view.setVisibility(View.VISIBLE);
            view.setText(text);
        } else {
            view.setText("");  // yes, it is necessary
            view.setVisibility(View.GONE);
        }
    }

    static class ResultViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextTranslation;
        public TextView mTextDefinition;
        public CheckBox mCheckBox;

        public ResultViewHolder(View itemView) {
            super(itemView);
            mTextTranslation = (TextView) itemView.findViewById(R.id.search_result_translation);
            mTextDefinition = (TextView) itemView.findViewById(R.id.search_result_definition);
            mCheckBox = (CheckBox) itemView.findViewById(R.id.result_checkbox);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCheckBox.performClick();
                }
            });
        }
    }

}
