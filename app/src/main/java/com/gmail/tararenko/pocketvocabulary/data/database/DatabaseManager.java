package com.gmail.tararenko.pocketvocabulary.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gmail.tararenko.pocketvocabulary.data.database.table.WordsTable;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public DatabaseManager(Context mContext) {
        this.mContext = mContext;
        openConnection();
    }

    public long addWord(WordModel word) {
        ContentValues values = generateValuesFromWordModel(word);
        return mDatabase.insert(WordsTable.TABLE_NAME, WordsTable.COLUMN_NAME_NULLABLE, values);
    }

    public List<WordModel> getWords() {
        Cursor cursor = mDatabase.query(WordsTable.TABLE_NAME, WordsTable.COLUMNS,
                null, null, null, null, null);
        List<WordModel> result = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                result.add(getWordFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        return result;
    }


    public List<WordModel> getWordsSortByCounter() {
        Cursor cursor = mDatabase.query(WordsTable.TABLE_NAME, WordsTable.COLUMNS,
                null, null, null, null, WordsTable.COLUMN_WORD_COUNTER + " ASC");
        List<WordModel> result = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                result.add(getWordFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        return result;
    }

    public List<WordModel> getWords(List<WordModel.WordType> types) {
        String[] typesAsStrings = new String[types.size()];
        for (int i = 0; i < types.size(); i++) {
            typesAsStrings[i] = types.get(i).getFieldDescription();
        }
        Cursor cursor = mDatabase.query(WordsTable.TABLE_NAME,
                WordsTable.COLUMNS,
                generateWhereStatementFromTypes(types),
                typesAsStrings,
                null, null, null);
        List<WordModel> result = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                result.add(getWordFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        return result;
    }

    public List<WordModel> getWords(String word) {
        Cursor cursor = mDatabase.query(WordsTable.TABLE_NAME,
                WordsTable.COLUMNS,
                WordsTable.COLUMN_WORD + " = ?",
                new String[] {word},
                null, null, null);
        List<WordModel> result = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                result.add(getWordFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        return result;
    }

    public WordModel getWord(long wordId) {
        Cursor cursor = mDatabase.query(WordsTable.TABLE_NAME,
                WordsTable.COLUMNS,
                WordsTable.COLUMN_WORD_ID + " = ?",
                new String[]{String.valueOf(wordId)},
                null, null, null);
        if (cursor.moveToFirst()) {
            return getWordFromCursor(cursor);
        }
        return null;
    }

    public long replaceWord(WordModel word) {
        ContentValues values = generateValuesFromWordModel(word);
        if (word.getWordId() > 0) {
            values.put(WordsTable.COLUMN_WORD_ID, word.getWordId());
        }
        return mDatabase.replace(WordsTable.TABLE_NAME, WordsTable.COLUMN_NAME_NULLABLE, values);
    }

    public long deleteWord(long wordId) {
        return mDatabase.delete(WordsTable.TABLE_NAME,
                WordsTable.COLUMN_WORD_ID + " = " + wordId,
                null);
    }

    public void mergeWords(List<WordModel> words) {
        for (WordModel word: words) {
            WordModel result = getWord(word.getWordId());
            if (result != null && !result.equals(word)) {
                addWord(word);
                continue;
            }
            List<WordModel> results = getWords(word.getWord());
            boolean found = false;
            for (WordModel w: results) {
                if (word.equals(w)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                addWord(word);
            }
        }
    }

    public void incCounter(WordModel word) {
        Cursor cursor = mDatabase.query(WordsTable.TABLE_NAME,
                WordsTable.COLUMNS,
                WordsTable.COLUMN_WORD_ID + " = ?",
                new String[]{String.valueOf(word.getWordId())},
                null, null, null);
        if (cursor.moveToFirst()) {
            WordModel w = getWordFromCursor(cursor);
            Log.d("DatabaseManager", "Founded word: " + w);
            ContentValues values = generateValuesFromWordModel(w);
            int counter = cursor.getInt(cursor.getColumnIndex(WordsTable.COLUMN_WORD_COUNTER));
            values.put(WordsTable.COLUMN_WORD_COUNTER, counter + 1);
            int c = mDatabase.update(WordsTable.TABLE_NAME,
                    values,
                    WordsTable.COLUMN_WORD_ID + " = ?",
                    new String[] {String.valueOf(w.getWordId())});
            Log.d("DatabaseManager", "replace word: " + w.getWordId() + " : " + c);
        }
    }

    public void openConnection() {
        if (mDatabase == null) {
            mDatabase = new DatabaseHelper(mContext).getWritableDatabase();
        } else if (!mDatabase.isOpen()) {
            mDatabase = new DatabaseHelper(mContext).getWritableDatabase();
        }
    }

    public void closeConnection() {
        mDatabase.close();
    }

    private String generateWhereStatementFromTypes(List<WordModel.WordType> types) {
        String result = "";
        for (int i = 0; i < types.size(); i++) {
            if (i != 0) {
                result += " OR ";
            }
            result += WordsTable.COLUMN_WORD_TYPE + " = ?";
        }
        return result.equals("") ? null : result;
    }

    private ContentValues generateValuesFromWordModel(WordModel word) {
        ContentValues values = new ContentValues();
        values.put(WordsTable.COLUMN_WORD, word.getWord());
        values.put(WordsTable.COLUMN_WORD_TYPE, word.getWordType().getFieldDescription());
        values.put(WordsTable.COLUMN_WORD_DEFINITION, word.getDefinition());
        values.put(WordsTable.COLUMN_WORD_TRANSLATION, word.getTranslation());
        values.put(WordsTable.COLUMN_WORD_EXAMPLE, word.getExample());
        values.put(WordsTable.COLUMN_WORD_EXAMPLE_TRANSLATION, word.getExampleTranslation());
        values.put(WordsTable.COLUMN_WORD_SYNONYM, word.getSynonym());
        return values;
    }

    private WordModel getWordFromCursor(Cursor cursor) {
        WordModel word = new WordModel(cursor.getInt(cursor.getColumnIndex(WordsTable.COLUMN_WORD_ID)));
        word.setWord(cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD)));
        word.setDefinition(cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD_DEFINITION)));
        word.setTranslation(cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD_TRANSLATION)));
        word.setExample(cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD_EXAMPLE)));
        word.setExampleTranslation(cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD_EXAMPLE_TRANSLATION)));
        word.setSynonym(cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD_SYNONYM)));
        String type = cursor.getString(cursor.getColumnIndex(WordsTable.COLUMN_WORD_TYPE)).toUpperCase();
        word.setWordType(WordModel.WordType.getEnumFromString(type));
        return word;
    }

}
