package com.gmail.tararenko.pocketvocabulary.data.database.table;

import android.provider.BaseColumns;

public class WordsTable implements BaseColumns {

    public static final String TABLE_NAME = "words";
    public static final String COLUMN_WORD_ID = "_id";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_WORD_TYPE = "type";
    public static final String COLUMN_WORD_DEFINITION = "definition";
    public static final String COLUMN_WORD_TRANSLATION = "translation";
    public static final String COLUMN_WORD_EXAMPLE = "example";
    public static final String COLUMN_WORD_EXAMPLE_TRANSLATION = "example_translation";
    public static final String COLUMN_WORD_SYNONYM = "synonym";
    public static final String COLUMN_WORD_CREATION_DATE = "creation_date";
    public static final String COLUMN_WORD_COUNTER = "counter";
    public static final String COLUMN_NAME_NULLABLE = "nullable";

    public static final String[] COLUMNS = {
            COLUMN_WORD_ID,
            COLUMN_WORD,
            COLUMN_WORD_TYPE,
            COLUMN_WORD_DEFINITION,
            COLUMN_WORD_TRANSLATION,
            COLUMN_WORD_EXAMPLE,
            COLUMN_WORD_EXAMPLE_TRANSLATION,
            COLUMN_WORD_SYNONYM,
            COLUMN_WORD_CREATION_DATE,
            COLUMN_WORD_COUNTER,
            COLUMN_NAME_NULLABLE
    };

}
