package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

public interface IWordEditPresenter {

    void onOptionsItemSelected(int itemId);

    WordModel getWord(long wordId);

    void findWord();

}
