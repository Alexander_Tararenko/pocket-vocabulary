package com.gmail.tararenko.pocketvocabulary.presentation.view;

import android.content.Context;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

public interface IWordEditView {

    Context getContext();

    WordModel getData();

    boolean validateData();

    void setErrorEmptyMessages();

    void onConfirmClicked(long wordId);

    void onBack();

    void showConnectionErrorDialog();

    void showProgress();

    void hideProgress();

    void showSearchResultDialog();

}
