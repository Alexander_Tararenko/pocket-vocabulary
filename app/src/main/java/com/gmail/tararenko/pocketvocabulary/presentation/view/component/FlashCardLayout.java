package com.gmail.tararenko.pocketvocabulary.presentation.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.gmail.tararenko.pocketvocabulary.R;

/**
 ______  __      ______  ______  ______  ______       __     __  ______  _____  __  __
/\  == \/\ \    /\  ___\/\  __ \/\  ___\/\  ___\     /\ \  _ \ \/\  __ \/\  == \/\ \/ /
\ \  _-/\ \ \___\ \  __\\ \  __ \ \___  \ \  __\     \ \ \/ ".\ \ \ \/\ \ \  __<\ \  _"-.
 \ \_\   \ \_____\ \_____\ \_\ \_\/\_____\ \_____\    \ \__/".~\_\ \_____\ \_\ \_\ \_\ \_\
  \/_/    \/_____/\/_____/\/_/\/_/\/_____/\/_____/     \/_/   \/_/\/_____/\/_/ /_/\/_/\/_/
                                                                                           
*/

public class FlashCardLayout extends FrameLayout {

    private LayoutInflater mInflater;

    public FlashCardLayout(Context context) {
        super(context);
        this.mInflater = LayoutInflater.from(context);
        mInflater.inflate(R.layout.flash_card_view, this, true);
    }

    public FlashCardLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mInflater = LayoutInflater.from(context);
        mInflater.inflate(R.layout.flash_card_view, this, true);
    }

    public FlashCardLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mInflater = LayoutInflater.from(context);
        mInflater.inflate(R.layout.flash_card_view, this, true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (height > width) { // device in portrait
            height = width;
        } else { // device in landscape
            width = (int) (height * 1.4);
        }
        setMeasuredDimension(width, height);
    }

}
