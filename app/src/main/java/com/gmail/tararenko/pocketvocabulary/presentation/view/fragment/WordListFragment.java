package com.gmail.tararenko.pocketvocabulary.presentation.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.IWordListPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.WordListPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IWordListView;
import com.gmail.tararenko.pocketvocabulary.presentation.view.adapter.WordListAdapter;
import com.gmail.tararenko.pocketvocabulary.presentation.view.component.DividerItemDecoration;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class WordListFragment extends Fragment implements IWordListView {

    private OnWordFragmentListener mCallback;
    private RecyclerView mListView;
    private WordListAdapter mAdapter;
    private IWordListPresenter mPresenter;
    private int mSortOrder;
    private SparseBooleanArray mFilter;
    private TextView mEmptyTextView;
    private FloatingActionButton mFAB;
    private WordListAdapter.OnItemClickListener mItemListener =
            new WordListAdapter.OnItemClickListener() {
                @Override
                public void onWordItemClicked(long wordId) {
                    if (WordListFragment.this.mPresenter != null) {
                        WordListFragment.this.mPresenter.onWordClicked(wordId);
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle(R.string.app_name);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_word_list, container, false);
        mEmptyTextView = (TextView) rootView.findViewById(R.id.word_list_empty_view);
        mFAB = (FloatingActionButton) rootView.findViewById(R.id.fab);
        mListView = (RecyclerView) rootView.findViewById(R.id.word_list);
        mListView.setLayoutManager(new LinearLayoutManager(getContext()));
        mListView.addItemDecoration(new DividerItemDecoration(
                ContextCompat.getDrawable(getContext(), R.drawable.line_divider)));
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPresenter == null) {
            mPresenter = new WordListPresenter(this);
        }
        List<WordModel> words = mPresenter.getWords(checkedItemsToTypesArray(mFilter));
        Collections.sort(words, generateComparator(mSortOrder));
        renderWords(words);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onAddClick();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnWordFragmentListener) {
            this.mCallback = (OnWordFragmentListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mCallback = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_word_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        mPresenter.onOptionsItemSelected(id);
        return super.onOptionsItemSelected(item);
    }

    public void renderWords(List<WordModel> words) {
        if (words != null) {
            if (this.mAdapter == null) {
                this.mAdapter = new WordListAdapter(getActivity(), words);
                this.mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        checkAdapterIsEmpty();
                    }
                });
            } else {
                this.mAdapter.setWords(words);
            }
            this.mAdapter.setListener(mItemListener);
            this.mListView.setAdapter(this.mAdapter);
            checkAdapterIsEmpty();
        }
    }

    @Override
    public void viewWord(long wordId) {
        if (this.mCallback != null) {
            this.mCallback.onWordClicked(wordId);
        }
    }

    @Override
    public void addNewWord() {
        if (this.mCallback != null) {
            this.mCallback.onAddNewWordClicked();
        }
    }

    @Override
    public void showSortByDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.sort_by_dialog_title)
                .setSingleChoiceItems(R.array.sort_by_dialog_items, mSortOrder, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        sort(checkedItemToSortOrder(selectedPosition));
                    }
                })
                .setNegativeButton(R.string.cancel, null).show();

    }

    @Override
    public void showFilterByDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.filter_by_dialog_title)
                .setMultiChoiceItems(R.array.parts_of_speech, sparseBooleanArrayToArray(mFilter), null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SparseBooleanArray selectedPositions = ((AlertDialog) dialog).getListView().getCheckedItemPositions();
                        mFilter = selectedPositions;
                        renderWords(mPresenter.getWords(checkedItemsToTypesArray(selectedPositions)));
                    }
                })
                .setNegativeButton(R.string.cancel, null).show();
    }

    @Override
    public void sort(int sortOrder) {
        if (this.mAdapter != null) {
            this.mAdapter.sort(generateComparator(sortOrder));
            this.mSortOrder = sortOrder;
        }
    }

    @Override
    public void runFlashCard() {
        this.mCallback.runFlashCard();
    }

    @Override
    public void showSettings() {
        this.mCallback.showSettings();
    }

    private Comparator<WordModel> generateComparator(int sortOrder) {
        Comparator<WordModel> comparator;
        switch (sortOrder) {
            case SORT_BY_WORD_DESCENDING:
                comparator = new Comparator<WordModel>() {
                    @Override
                    public int compare(WordModel lhs, WordModel rhs) {
                        return -1 * lhs.getWord().compareTo(rhs.getWord());
                    }
                };
                break;
            case SORT_BY_TIME_DESCENDING:
                comparator = new Comparator<WordModel>() {
                    @Override
                    public int compare(WordModel lhs, WordModel rhs) {
                        return Long.valueOf(rhs.getWordId()).compareTo(lhs.getWordId());
                    }
                };
                break;
            case SORT_BY_TIME_ASCENDING:
                comparator = new Comparator<WordModel>() {
                    @Override
                    public int compare(WordModel lhs, WordModel rhs) {
                        return Long.valueOf(lhs.getWordId()).compareTo(rhs.getWordId());
                    }
                };
                break;
            default: //default is ascending sort by word
                comparator = new Comparator<WordModel>() {
                    @Override
                    public int compare(WordModel lhs, WordModel rhs) {
                        return lhs.getWord().compareTo(rhs.getWord());
                    }
                };
                break;
        }
        return comparator;
    }

    @Override
    public Context getContext() {
        return this.getActivity();
    }

    private int checkedItemToSortOrder(int checkedItem) {
        int result;
        switch (checkedItem) {
            default:
                result = IWordListView.SORT_BY_WORD_ASCENDING;
                break;
            case 1:
                result = IWordListView.SORT_BY_WORD_DESCENDING;
                break;
            case 2:
                result = IWordListView.SORT_BY_TIME_ASCENDING;
                break;
            case 3:
                result = IWordListView.SORT_BY_TIME_DESCENDING;
                break;
        }
        return result;
    }

    private List<WordModel.WordType> checkedItemsToTypesArray(SparseBooleanArray items) {
        if (items == null) {
            return null;
        }
        List<WordModel.WordType> types = new LinkedList<>();
        String[] values = getResources().getStringArray(R.array.parts_of_speech);
        for (int i = 0; i < items.size(); i++) {
            if (items.valueAt(i)) {
                types.add(WordModel.WordType.getEnumFromString(values[items.keyAt(i)]));
            }
        }
        return types;
    }

    private boolean[] sparseBooleanArrayToArray(SparseBooleanArray items) {
        if (mFilter == null) {
            return null;
        }
        int size = getResources().getStringArray(R.array.parts_of_speech).length;
        boolean[] result = new boolean[size];
        for (int i = 0; i < size; i++) {
            result[i] = items.get(i);
        }
        return result;
    }

    private void checkAdapterIsEmpty() {
        if (mAdapter.getItemCount() == 0) {
            mEmptyTextView.setVisibility(View.VISIBLE);
        } else {
            mEmptyTextView.setVisibility(View.GONE);
        }
    }

    public interface OnWordFragmentListener {

        void onWordClicked(long wordId);

        void onAddNewWordClicked();

        void runFlashCard();

        void showSettings();

    }


}
