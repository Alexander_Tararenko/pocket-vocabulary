package com.gmail.tararenko.pocketvocabulary.presentation.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.IWordDetailsPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.WordDetailsPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IWordDetailsView;

// TODO: text sometimes like to swim
public class WordDetailsFragment extends Fragment implements IWordDetailsView {

    private static final String FRAGMENT_EXTRA_PARAM_WORD_ID = "com.gmail.tarlorien.awesomevocabulary.INTENT_PARAM_WORD_ID";
    private static final String SPLIT_WORDS_REG_EXP = "[.;!?]";
    private static final String SPLIT_SENTENCES_REG_EXP = "[.!?]";

    private OnFragmentInteractionListener mCallback;
    private long mWordId;
    private WordModel mWord;
    private IWordDetailsPresenter mPresenter;

    private TextView mTextWord;
    private TextView mTextType;
    private TextView mTextTranslation;
    private TextView mTextDefinition;
    private TextView mTextExample;
    private TextView mTextExampleTranslation;
    private TextView mTextSynonym;
    private TextView mTextLabelTranslation;
    private TextView mTextLabelDefinition;
    private TextView mTextLabelExample;
    private TextView mTextLabelExampleTranslation;
    private TextView mTextLabelSynonym;

    public WordDetailsFragment() {
        super();
    }

    public static WordDetailsFragment getInstance(long wordId) {
        WordDetailsFragment fragment = new WordDetailsFragment();
        Bundle args = new Bundle();
        args.putLong(FRAGMENT_EXTRA_PARAM_WORD_ID, wordId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_details);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_word_details, container, false);
        mTextWord = (TextView) rootView.findViewById(R.id.details_word); // What the mess...
        mTextType = (TextView) rootView.findViewById(R.id.details_type);
        mTextTranslation = (TextView) rootView.findViewById(R.id.details_translation);
        mTextDefinition = (TextView) rootView.findViewById(R.id.details_definition);
        mTextExample = (TextView) rootView.findViewById(R.id.details_example);
        mTextExampleTranslation = (TextView) rootView.findViewById(R.id.details_example_translation);
        mTextSynonym = (TextView) rootView.findViewById(R.id.details_synonym);
        mTextLabelTranslation = (TextView) rootView.findViewById(R.id.details_translation_label);
        mTextLabelDefinition = (TextView) rootView.findViewById(R.id.details_definition_label);
        mTextLabelExample = (TextView) rootView.findViewById(R.id.details_example_label);
        mTextLabelExampleTranslation = (TextView) rootView.findViewById(R.id.details_example_translation_label);
        mTextLabelSynonym = (TextView) rootView.findViewById(R.id.details_synonym_label);
        Bundle args = getArguments();
        this.mWordId = args.getLong(FRAGMENT_EXTRA_PARAM_WORD_ID);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (this.mPresenter == null) {
            this.mPresenter = new WordDetailsPresenter(this);
            if (this.mWord == null) {
                this.mWord = this.mPresenter.getWord(mWordId);
            }
        }
        fillData(this.mWord);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onBack() {
        getActivity().onBackPressed();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_word_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        mPresenter.onOptionsItemSelected(id);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showConfirmDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.delete_message)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPresenter.deleteWord(mWordId);
                        mCallback.onDelete();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    public void fillData(WordModel word) {
        mTextWord.setText(word.getWord());
        mTextType.setText(getWordTypeAsString(word.getWordType()));
        fillField(mTextTranslation, mTextLabelTranslation,
                splitText(word.getTranslation(), SPLIT_WORDS_REG_EXP));
        fillField(mTextDefinition, mTextLabelDefinition,
                splitText(word.getDefinition(), SPLIT_WORDS_REG_EXP));
        fillField(mTextExample, mTextLabelExample,
                splitText(word.getExample(), SPLIT_SENTENCES_REG_EXP));
        fillField(mTextExampleTranslation, mTextLabelExampleTranslation,
                splitText(word.getExampleTranslation(), SPLIT_SENTENCES_REG_EXP));
        fillField(mTextSynonym, mTextLabelSynonym,
                splitText(word.getSynonym(), SPLIT_WORDS_REG_EXP));
    }

    private void fillField(TextView view, TextView label, String text) {
        if (text == null || text.isEmpty()) {
            view.setVisibility(View.GONE);
            label.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
            label.setVisibility(View.VISIBLE);
            view.setText(text);
        }
    }

    private String splitText(String text, String splitRegExp) {
        if (text == null) {
            return "";
        }
        String[] parts = text.split(splitRegExp);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            if (i > 0) {
                builder.append("\n");
            }
            builder.append(parts[i].replaceAll(splitRegExp, "").trim());
            if (splitRegExp.equals(SPLIT_SENTENCES_REG_EXP)) {
                builder.append(".");
            }
        }
        return builder.toString();
    }

    @Override
    public void editWord() {
        this.mCallback.onEdit(this.mWordId);
    }

    @Override
    public Context getContext() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        return actionBar == null ? this.getContext() : actionBar.getThemedContext();
    }

    @Override
    public long getWordId() {
        return this.mWordId;
    }

    private String getWordTypeAsString(WordModel.WordType type) {
        String[] types = getContext().getResources().getStringArray(R.array.parts_of_speech);
        switch (type) {
            case NOUN:
                return types[0];
            case VERB:
                return types[1];
            case IDIOM:
                return types[2];
            case PARTICIPLE:
                return types[3];
            case ADJECTIVE:
                return types[4];
            case ADVERB:
                return types[5];
            case PHRASAL_VERB:
                return types[6];
            case PREPOSITION:
                return types[7];
        }
        return "";
    }

    public interface OnFragmentInteractionListener {

        void onEdit(long wordId);

        void onDelete();

    }

}
