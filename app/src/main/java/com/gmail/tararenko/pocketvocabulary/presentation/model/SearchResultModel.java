package com.gmail.tararenko.pocketvocabulary.presentation.model;

import java.util.Comparator;

public class SearchResultModel {

    private final static int CHAR_LIMIT = 50;

    public final String translation;
    public final String definition;

    public SearchResultModel(String translation, String definition) {
        this.translation = translation == null ? "" : translation.trim();
        this.definition = definition == null ? "" : definition.trim();
    }

    public static Comparator<SearchResultModel> getSpecificComparator() {
        return new Comparator<SearchResultModel>() {

            @Override
            public int compare(SearchResultModel lhs, SearchResultModel rhs) {
                int result = hasTranslations(lhs.translation, rhs.translation);

                return result == 0 ? compareDefinitionLengths(lhs.definition, rhs.definition) : result;
            }

            private int hasTranslations(String lhs, String rhs) {
                return lhs.isEmpty() ? (rhs.isEmpty() ? 0 : -1) : (rhs.isEmpty() ? 1 : 0);
            }

            private int compareDefinitionLengths(String lhs, String rhs) {
                int result = rhs.length() - lhs.length();
                return result == 0 ? lhs.compareTo(rhs) : result;
            }

        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchResultModel that = (SearchResultModel) o;
        return translation.equals(that.translation) && definition.equals(that.definition);
    }

    @Override
    public int hashCode() {
        int result = translation.hashCode();
        result = 31 * result + definition.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SearchResultModel{" +
                "translation='" + translation + '\'' +
                ", definition='" + definition + '\'' +
                '}';
    }

}
