package com.gmail.tararenko.pocketvocabulary.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gmail.tararenko.pocketvocabulary.data.database.table.WordsTable;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "database.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL.CREATE_TABLE_WORDS);
        addExample(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL.DELETE_TABLE_WORDS);
        onCreate(db);
    }

    public void addExample(SQLiteDatabase db) {
        WordModel word = new WordModel(0);
        word.setWord("Example");
        word.setWordType(WordModel.WordType.NOUN);
        word.setDefinition("Something serving to explain or illustrate a rule; Something serving as a pattern of behaviour");
        word.setTranslation("Ejemplo; Ejemplar");
        word.setExample("Give me an example. For example, primary health-care centres exist in all the LGAs of the country.");
        word.setExampleTranslation("Dame un ejemplo. Por ejemplo, en todas las administraciones locales hay centros de atención primaria de la salud.");
        addWord(db, word);
        word = new WordModel(1);
        word.setWord("Far cry");
        word.setWordType(WordModel.WordType.IDIOM);
        word.setDefinition("(idiomatic) Something very dissimilar or different");
        word.setExample("Believe me, it is a far cry from anything but beautiful.");
        word.setExampleTranslation("Créeme, está muy lejos de no ser precioso.");
        addWord(db, word);
    }

    public void addWord(SQLiteDatabase db, WordModel word) {
        ContentValues values = new ContentValues();
        values.put(WordsTable.COLUMN_WORD, word.getWord());
        values.put(WordsTable.COLUMN_WORD_TYPE, word.getWordType().getFieldDescription());
        values.put(WordsTable.COLUMN_WORD_DEFINITION, word.getDefinition());
        values.put(WordsTable.COLUMN_WORD_TRANSLATION, word.getTranslation());
        values.put(WordsTable.COLUMN_WORD_EXAMPLE, word.getExample());
        values.put(WordsTable.COLUMN_WORD_EXAMPLE_TRANSLATION, word.getExampleTranslation());
        values.put(WordsTable.COLUMN_WORD_SYNONYM, word.getSynonym());
        db.insert(WordsTable.TABLE_NAME, WordsTable.COLUMN_NAME_NULLABLE, values);
    }

}
