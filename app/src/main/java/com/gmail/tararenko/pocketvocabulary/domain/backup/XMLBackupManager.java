package com.gmail.tararenko.pocketvocabulary.domain.backup;

import android.content.Context;
import android.os.Environment;
import android.util.Xml;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.data.database.DatabaseManager;
import com.gmail.tararenko.pocketvocabulary.domain.backup.parser.FeedParser;
import com.gmail.tararenko.pocketvocabulary.domain.backup.parser.XmlPullFeedParser;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

public class XMLBackupManager implements IBackupManager {

    public static final String FILE_NAME = "backup.xml";
    private Context mContext;

    public XMLBackupManager(Context context) {
        this.mContext = context;
    }

    @Override
    public boolean makeBackup() {
        // Why FileWriter class doesn't work?
        DatabaseManager manager = new DatabaseManager(mContext);
        PrintWriter printWriter = null;
        try {
            File backupFile = createFile();
            printWriter = new PrintWriter(new BufferedWriter(new FileWriter(backupFile)));
            String xml = generateXMLString(manager.getWords());
            printWriter.write(xml);
            printWriter.close();
        } catch (IOException e) {
            return false;
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
            manager.closeConnection();
        }
        return true;
    }

    @Override
    public boolean restoreFromBackup() {
        File backupFile = getFile();
        XmlPullFeedParser parser = new XmlPullFeedParser(backupFile);
        DatabaseManager manager = new DatabaseManager(mContext);
        try {
            List<WordModel> words = parser.parse();
            manager.mergeWords(words);
        } catch (XmlPullParserException e) {
            return false;
        } catch (FileNotFoundException e) {
            return false;
        } finally {
            manager.closeConnection();
        }
        return true;
    }

    private File createFile() throws IOException {
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath()
                + "/" + mContext.getString(R.string.app_name) + "/backup");
        dir.mkdirs();
        File backupFile = new File(dir, FILE_NAME);
        if (backupFile.exists()) {
            backupFile.delete();
        }
        backupFile.createNewFile();
        return backupFile;
    }

    private File getFile() {
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath()
                + "/" + mContext.getString(R.string.app_name) + "/backup");
        return new File(dir, FILE_NAME);
    }

    private String generateXMLString(List<WordModel> items) throws IOException{
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        StringWriter writer = new StringWriter();
        serializer.setOutput(writer);
        serializer.startDocument("UTF-8", true);
        serializer.startTag("", FeedParser.TAG_WORDS);
        serializer.attribute("", FeedParser.ATTRIBUTE_NUMBER, String.valueOf(items.size()));
        for (WordModel word: items) {
            serializer.startTag("", FeedParser.TAG_ENTRY);
            serializer.attribute("", FeedParser.ATTRIBUTE_ID, String.valueOf(word.getWordId()));
            fillTag(serializer, FeedParser.TAG_WORD, word.getWord());
            fillTag(serializer, FeedParser.TAG_TYPE, word.getWordType().getFieldDescription());
            fillTag(serializer, FeedParser.TAG_TRANSLATION, word.getTranslation());
            fillTag(serializer, FeedParser.TAG_DEFINITION, word.getDefinition());
            fillTag(serializer, FeedParser.TAG_EXAMPLE, word.getExample());
            fillTag(serializer, FeedParser.TAG_EXAMPLE_TRANSLATION, word.getExampleTranslation());
            fillTag(serializer, FeedParser.TAG_SYNONYM, word.getSynonym());
            serializer.endTag("", FeedParser.TAG_ENTRY);
        }
        serializer.endTag("", FeedParser.TAG_WORDS);
        serializer.endDocument();
        return writer.toString();
    }

    private void fillTag(XmlSerializer serializer, String tag, String text) throws IOException {
        if (text == null || text.isEmpty()) {
            return;
        }
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        serializer.startTag("", tag);
        serializer.text(text);
        serializer.endTag("", tag);
    }

}
