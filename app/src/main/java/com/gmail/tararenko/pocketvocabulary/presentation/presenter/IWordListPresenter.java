package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import java.util.List;

public interface IWordListPresenter {

    List<WordModel> getWords();

    List<WordModel> getWords(List<WordModel.WordType> types);

    void onWordClicked(long wordId);

    void onOptionsItemSelected(int id);

    void onAddClick();

}
