package com.gmail.tararenko.pocketvocabulary.presentation.view;

import android.content.Context;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import java.util.List;

public interface IWordListView {

    int SORT_BY_WORD_ASCENDING = 0;
    int SORT_BY_WORD_DESCENDING = 1;
    int SORT_BY_TIME_ASCENDING = 2;
    int SORT_BY_TIME_DESCENDING = 3;

    void renderWords(List<WordModel> words);

    void sort(int sortOrder);

    Context getContext();

    void viewWord(long wordId);

    void showSortByDialog();

    void showFilterByDialog();

    void addNewWord();

    void runFlashCard();

    void showSettings();

}
