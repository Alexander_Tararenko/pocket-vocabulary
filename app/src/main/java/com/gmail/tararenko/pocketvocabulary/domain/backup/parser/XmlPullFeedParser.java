package com.gmail.tararenko.pocketvocabulary.domain.backup.parser;

import android.util.Xml;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class XmlPullFeedParser implements FeedParser {

    private File backupFile;

    public XmlPullFeedParser(File backupFile) {
        this.backupFile = backupFile;
    }

    @Override
    public List<WordModel> parse() throws XmlPullParserException, FileNotFoundException {
        XmlPullParser parser = initParser();
        return parseWords(parser);
    }

    private XmlPullParser initParser() throws XmlPullParserException, FileNotFoundException{
        XmlPullParser parser = Xml.newPullParser();
        FileInputStream inputStream = new FileInputStream(backupFile);
        parser.setInput(inputStream, "UTF-8");
        return parser;
    }

    private List<WordModel> parseWords(XmlPullParser parser) throws XmlPullParserException {
        int eventType = parser.getEventType();
        List<WordModel> words = new LinkedList<>();
        WordModel word = null;
        String name = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase(FeedParser.TAG_ENTRY)) {
                        long id = Long.parseLong(parser.getAttributeValue("", FeedParser.ATTRIBUTE_ID));
                        word = new WordModel(id);
                    }
                    break;
                case XmlPullParser.TEXT:
                    parseTag(word, name, parser.getText());
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase(FeedParser.TAG_ENTRY) && word != null) {
                        words.add(word);
                    }
                    break;
            }
            try {
                eventType = parser.next();
            } catch (IOException e) {
                return words;
            }
        }

        return words;
    }

    private void parseTag(WordModel word, String tag, String text) {
        if (word == null || text == null || text.trim().isEmpty()) {
            return;
        }
        if (tag.equalsIgnoreCase(FeedParser.TAG_TYPE)) {
            word.setWordType(WordModel.WordType.getEnumFromString(text));
        } else if (tag.equalsIgnoreCase(FeedParser.TAG_WORD)) {
            word.setWord(text);
        } else if (tag.equalsIgnoreCase(FeedParser.TAG_TRANSLATION)) {
            word.setTranslation(text);
        } else if (tag.equalsIgnoreCase(FeedParser.TAG_DEFINITION)) {
            word.setDefinition(text);
        } else if (tag.equalsIgnoreCase(FeedParser.TAG_EXAMPLE)) {
            word.setExample(text);
        } else if (tag.equalsIgnoreCase(FeedParser.TAG_EXAMPLE_TRANSLATION)) {
            word.setExampleTranslation(text);
        } else if (tag.equalsIgnoreCase(FeedParser.TAG_SYNONYM)) {
            word.setSynonym(text);
        }
    }

    public void setBackupFile(File backupFile) {
        this.backupFile = backupFile;
    }
}
