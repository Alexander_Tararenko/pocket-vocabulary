package com.gmail.tararenko.pocketvocabulary.presentation.presenter;


import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

public interface IFlashCardPresenter {

    void onOptionsItemSelected(int itemId);

    WordModel getCurrentWord();

    WordModel nextWord();

    void toggle();

    void refreshWords();

    void incCounter(WordModel word);

}
