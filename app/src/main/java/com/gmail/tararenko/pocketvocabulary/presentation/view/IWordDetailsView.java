package com.gmail.tararenko.pocketvocabulary.presentation.view;

import android.content.Context;

public interface IWordDetailsView {

    Context getContext();

    void showConfirmDeleteDialog();

    void editWord();

    long getWordId();

    void onBack();

}
