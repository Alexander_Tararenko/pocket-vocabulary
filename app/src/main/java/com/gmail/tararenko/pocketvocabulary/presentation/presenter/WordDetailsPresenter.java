package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.data.database.DatabaseManager;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IWordDetailsView;

public class WordDetailsPresenter implements IWordDetailsPresenter {

    private IWordDetailsView mView;

    public WordDetailsPresenter(IWordDetailsView view) {
        this.mView = view;
    }

    @Override
    public WordModel getWord(long wordId) {
        DatabaseManager manager = new DatabaseManager(mView.getContext());
        WordModel result = manager.getWord(wordId);
        manager.closeConnection();
        return result;
    }

    @Override
    public void deleteWord(long wordId) {
        DatabaseManager manager = new DatabaseManager(mView.getContext());
        manager.deleteWord(wordId);
        manager.closeConnection();
    }

    @Override
    public void onOptionsItemSelected(int id) {
        switch (id) {
            case R.id.action_edit:
                mView.editWord();
                break;
            case R.id.action_delete:
                mView.showConfirmDeleteDialog();
                break;
            case android.R.id.home:
                mView.onBack();
                break;
        }
    }

}
