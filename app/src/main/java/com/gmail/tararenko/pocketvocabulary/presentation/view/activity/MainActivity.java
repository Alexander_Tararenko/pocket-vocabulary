package com.gmail.tararenko.pocketvocabulary.presentation.view.activity;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.presentation.view.component.ThemeMapping;
import com.gmail.tararenko.pocketvocabulary.presentation.view.fragment.FlashCardFragment;
import com.gmail.tararenko.pocketvocabulary.presentation.view.fragment.SettingsFragment;
import com.gmail.tararenko.pocketvocabulary.presentation.view.fragment.WordDetailsFragment;
import com.gmail.tararenko.pocketvocabulary.presentation.view.fragment.WordEditFragment;
import com.gmail.tararenko.pocketvocabulary.presentation.view.fragment.WordListFragment;

/**
 * When you create something, you invest in this part of the soul (c)
 */
public class MainActivity extends AppCompatActivity
        implements WordListFragment.OnWordFragmentListener,
        WordDetailsFragment.OnFragmentInteractionListener,
        WordEditFragment.OnFragmentInteractionListener,
        FlashCardFragment.OnFragmentInteractionListener {

    private static final String FRAGMENT_TAG = "com.gmail.tarlorien.awesomevocabulary.WORD_FRAGMENT";
    private static final String BACK_STACK_TAG = "com.gmail.tarlorien.awesomevocabulary.MainActivity";
    private static int moves = 0;
    private Toolbar mToolbar;
    private Fragment mFragment;
    private boolean stackFlag = false;
    private boolean showRateDialog = false;

    public static Intent getCallingIntent(Context context) {
        Intent callingIntent = new Intent(context, MainActivity.class);
        return callingIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initTheme();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        FragmentManager fragmentManager = getSupportFragmentManager();
        this.mFragment = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if (this.mFragment == null) {
            fragmentManager.beginTransaction()
                    .add(R.id.container_body, new WordListFragment(), FRAGMENT_TAG)
                    .commit();
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        showRateDialog = preferences.getBoolean("showRateDialog", false);
    }

    @Override
    public void onWordClicked(long wordId) {
        showDetailsFragment(wordId, true);
    }

    @Override
    public void onEdit(long wordId) {
        showEditFragment(wordId);
    }

    @Override
    public void onDelete() {
        showListFragment();
    }

    @Override
    public void onConfirm(long wordId) {
        if (wordId < 0) {
            showListFragment();
        } else {
            showDetailsFragment(wordId, true);
        }
    }

    @Override
    public void onAddNewWordClicked() {
        showEditFragment(-1);
    }

    @Override
    public void runFlashCard() {
        showFlashCardFragment();
    }

    @Override
    public void showSettings() {
        showSettingsFragment();
    }

    public void showDetailsFragment(long wordId, boolean addToBackStack) {
        replaceFragment(WordDetailsFragment.getInstance(wordId), addToBackStack);
    }

    public void showEditFragment(long wordId) {
        replaceFragment(WordEditFragment.getInstance(wordId), true);
    }

    public void showListFragment() {
        replaceFragment(new WordListFragment(), false);
    }

    public void showFlashCardFragment() {
        replaceFragment(new FlashCardFragment(), true);
    }

    public void showSettingsFragment() {
        replaceFragment(new SettingsFragment(), true);
    }

    @SuppressLint("CommitTransaction")
    private void replaceFragment(Fragment fragment, boolean addToBackStack) {
        if (fragment instanceof WordDetailsFragment) {
            stackFlag = true;
        } else {
            stackFlag = false;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.grow_fade_in_from_bottom, R.anim.fade_out,
                        R.anim.fade_in, R.anim.shrink_fade_out_from_bottom)
                .replace(R.id.container_body, fragment, FRAGMENT_TAG);
        if (addToBackStack) {
            transaction.addToBackStack(BACK_STACK_TAG);
        }
        this.mFragment = fragment;
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        moves++;
        if (stackFlag) {
            returnToListFragment();
        } else {
            super.onBackPressed();
        }
        if (showRateDialog && moves > 3) {
            createRateDialog();
        }
    }

    private void returnToListFragment() {
        stackFlag = false;
        this.mFragment = new WordListFragment();
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.shrink_fade_out_from_bottom,
                        0, 0)
                .replace(R.id.container_body, this.mFragment, FRAGMENT_TAG)
                .commit();
    }

    private void initTheme() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String theme = preferences.getString(ThemeMapping.THEME_NAME_KEY, "");
        switch (theme) {
            case ThemeMapping.BLUEGRAY_THEME:
                setTheme(R.style.BlueGrayTheme);
                break;
            case ThemeMapping.RED_THEME:
                setTheme(R.style.RedTheme);
                break;
            default:
                setTheme(R.style.BlueGrayTheme);
        }
    }

    private void createRateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        final Context context = this;
        builder.setTitle(getString(R.string.rate_app_dialog_title))
                .setMessage(getString(R.string.rate_app_dialog_message))
                .setPositiveButton(getString(R.string.rate_app_dialog_positive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        preferences.edit().putBoolean("rated", true).apply();
                        preferences.edit().putBoolean("showRateDialog", false).apply();
                        showRateDialog = false;
                        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                        }
                    }
                })
                .setNegativeButton(getString(R.string.rate_app_dialog_negative), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        preferences.edit().putBoolean("rated", true).apply();
                        preferences.edit().putBoolean("showRateDialog", false).apply();
                        showRateDialog = false;
                    }
                })
                .setNeutralButton(getString(R.string.rate_app_dialog_neutral), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        preferences.edit().putInt("launches", 0).apply();
                        preferences.edit().putBoolean("showRateDialog", false).apply();
                        showRateDialog = false;
                    }
                })
                .show();
    }

}