package com.gmail.tararenko.pocketvocabulary.presentation.navigation;

import android.content.Context;
import android.content.Intent;

import com.gmail.tararenko.pocketvocabulary.presentation.view.activity.MainActivity;

public class Navigator {

    private static Navigator ourInstance = new Navigator();

    private Navigator() {
    }

    public static Navigator getInstance() {
        return ourInstance;
    }

    public void navigateToMainActivity(Context context) {
        if (context != null) {
            Intent intentToLaunch = MainActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

}
