package com.gmail.tararenko.pocketvocabulary.presentation.view.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.domain.util.Languages;
import com.gmail.tararenko.pocketvocabulary.presentation.model.SearchResultModel;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.IWordEditPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.presenter.WordEditPresenter;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IWordEditView;

import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

public class WordEditFragment extends Fragment
        implements IWordEditView, SearchResultsDialogFragment.SearchResultsDialogListener {

    private static final String FRAGMENT_EXTRA_PARAM_WORD_ID = "com.gmail.tarlorien.awesomevocabulary.INTENT_PARAM_WORD_ID";
    private static final String DIALOG_FRAGMENT_TAG = "com.gmail.tarlorien.awesomevocabulary.SEARCH_FRAGMENT_TAG";

    private OnFragmentInteractionListener mCallback;
    private long mWordId;
    private IWordEditPresenter mPresenter;
    private WordModel mWord;
    private String from;
    private String to;

    private EditText mEditTextWord;
    private EditText mEditTextDefinition;
    private EditText mEditTextTranslation;
    private EditText mEditTextExample;
    private EditText mEditTextExampleTranslation;
    private EditText mEditTextSynonym;
    private Spinner mSpinnerType;
    private ImageButton mButtonSearch;
    private ArrayAdapter<CharSequence> mSpinnerAdapter;

    public static WordEditFragment getInstance(long wordId) {
        WordEditFragment fragment = new WordEditFragment();
        Bundle args = new Bundle();
        args.putLong(FRAGMENT_EXTRA_PARAM_WORD_ID, wordId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        this.from = Languages.getLocale(preferences.getString(getString(R.string.preferences_translation_from_key), "English")).getISO3Language();
        this.to = Languages.getLocale(preferences.getString(getString(R.string.preferences_translation_to_key), "Spanish")).getISO3Language();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            if (this.mWordId > 0) {
                actionBar.setTitle(R.string.title_edit_word);
            } else {
                actionBar.setTitle(R.string.title_new_word);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_word_edit, container, false);
        this.mEditTextWord = (EditText) rootView.findViewById(R.id.word_edit_word);
        this.mEditTextDefinition = (EditText) rootView.findViewById(R.id.word_edit_definition);
        this.mEditTextTranslation = (EditText) rootView.findViewById(R.id.word_edit_translation);
        this.mEditTextExample = (EditText) rootView.findViewById(R.id.word_edit_example);
        this.mEditTextExampleTranslation = (EditText) rootView.findViewById(R.id.word_edit_example_translation);
        this.mEditTextSynonym = (EditText) rootView.findViewById(R.id.word_edit_synonym);
        this.mSpinnerType = (MaterialSpinner) rootView.findViewById(R.id.word_spinner_types);
        this.mButtonSearch = (ImageButton) rootView.findViewById(R.id.search);
        this.mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditTextWord.getText().toString().trim().isEmpty()) {
                    String errorMessage = getString(R.string.error_empty);
                    mEditTextWord.setError(errorMessage);
                } else {
                    mPresenter.findWord();
                }
            }
        });
        Bundle args = getArguments();
        this.mWordId = args.getLong(FRAGMENT_EXTRA_PARAM_WORD_ID);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mSpinnerAdapter = ArrayAdapter.createFromResource(
                getContext(), R.array.parts_of_speech, android.R.layout.simple_spinner_item);
        this.mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerType.setAdapter(this.mSpinnerAdapter);
        if (this.mPresenter == null) {
            this.mPresenter = new WordEditPresenter(this);
        }
        this.mWord = this.mPresenter.getWord(this.mWordId);
        if (this.mWord != null) {
            fillData(this.mWord);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager keyboard = (InputMethodManager)
                    getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBack() {
        getActivity().onBackPressed();
    }

    @Override
    public Context getContext() {
        return this.getActivity();
    }

    @Override
    public WordModel getData() {
        WordModel word = new WordModel(this.mWordId);
        word.setWord(mEditTextWord.getText().toString());
        word.setDefinition(mEditTextDefinition.getText().toString());
        word.setTranslation(mEditTextTranslation.getText().toString());
        word.setExample(mEditTextExample.getText().toString());
        word.setExampleTranslation(mEditTextExampleTranslation.getText().toString());
        word.setSynonym(mEditTextSynonym.getText().toString());
        word.setWordType(WordModel.WordType.getEnumFromNumber(mSpinnerType.getSelectedItemPosition()));
        return word;
    }

    @Override
    public boolean validateData() {
        boolean result = isEditTextEmpty(mEditTextWord);
        result = result && (isEditTextEmpty(mEditTextDefinition) || isEditTextEmpty(mEditTextTranslation));
        return result;
    }

    @Override
    public void setErrorEmptyMessages() {
        String errorMessage = getString(R.string.error_empty);
        if (mEditTextWord.getText().toString().isEmpty()) {
            mEditTextWord.setError(errorMessage);
        }
        if (mEditTextDefinition.getText().toString().isEmpty()) {
            mEditTextDefinition.setError(errorMessage);
        }
        if (mEditTextTranslation.getText().toString().isEmpty()) {
            mEditTextTranslation.setError(errorMessage);
        }
    }

    @Override
    public void showSearchResultDialog() {
        SearchResultsDialogFragment fragment = SearchResultsDialogFragment.getInstance(mEditTextWord.getText().toString().trim(), from, to);
        fragment.setCallback(this);
        fragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
    }

    @Override
    public void showConnectionErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.dialog_error_no_connection_title)
                .setMessage(R.string.dialog_error_no_connection_message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mButtonSearch.performClick();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    @Override
    public void showProgress() {
        this.mButtonSearch.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        this.mButtonSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_word_edit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        mPresenter.onOptionsItemSelected(id);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfirmClicked(long wordId) {
        this.mCallback.onConfirm(wordId);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onConfirm(List<SearchResultModel> results) {
        appendData(results);
    }

    private void appendData(List<SearchResultModel> data) {
        for (int i = 0; i < data.size(); i++) {
            appendText(this.mEditTextDefinition, data.get(i).definition);
            appendText(this.mEditTextTranslation, data.get(i).translation);
        }
    }

    private void appendText(EditText view, String text) {
        if (text == null || view == null) {
            return;
        }
        if (view.getText().length() == 0) {
            view.setText(text);
        } else {
            view.append("; " + text);
        }
    }

    private void fillData(WordModel word) {
        this.setTextToEditText(mEditTextWord, word.getWord());
        this.setTextToEditText(mEditTextDefinition, word.getDefinition());
        this.setTextToEditText(mEditTextTranslation, word.getTranslation());
        this.setTextToEditText(mEditTextExample, word.getExample());
        this.setTextToEditText(mEditTextExampleTranslation, word.getExampleTranslation());
        this.setTextToEditText(mEditTextSynonym, word.getSynonym());
        this.mSpinnerType.setSelection(word.getWordType().getPosition());
    }

    private void setTextToEditText(EditText editText, String text) {
        if (text != null) {
            editText.setText(text);
        }
    }

    private boolean isEditTextEmpty(EditText view) {
        return !view.getText().toString().trim().isEmpty();
    }

    public interface OnFragmentInteractionListener {

        void onConfirm(long wordId);

    }

}
