package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import com.gmail.tararenko.pocketvocabulary.data.database.DatabaseManager;
import com.gmail.tararenko.pocketvocabulary.domain.util.Random;
import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;
import com.gmail.tararenko.pocketvocabulary.presentation.view.IFlashCardView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FlashCardPresenter implements IFlashCardPresenter {

    int currentPosition;
    private IFlashCardView mView;
    private List<WordModel> words;
    private int quantity;
    private DatabaseManager manager;

    public FlashCardPresenter(IFlashCardView mView, int quantity) {
        this.mView = mView;
        this.quantity = quantity;
        getWords();
        currentPosition = 0;
    }

    @Override
    public void toggle() {
        if (mView.isFront()) {
            mView.showBack();
        } else {
            if (nextWord() != null) {
                mView.showNext(getCurrentWord());
            } else {
                mView.showEnd();
            }
        }
    }

    @Override
    public WordModel getCurrentWord() {
        if (words == null) {
            getWords();
        }
        return words.get(currentPosition);
    }

    @Override
    public WordModel nextWord() {
        if (words == null) {
            getWords();
        }
        if ((currentPosition + 1) < words.size()) {
            return words.get(++currentPosition);
        } else {
            return null;
        }
    }

    @Override
    public void onOptionsItemSelected(int itemId) {
        switch (itemId) {
            case android.R.id.home:
                mView.onBack();
                break;
        }
    }

    private void getWords() {
        openDatabase();
        words = manager.getWordsSortByCounter();
        chooseWords(this.words);
        currentPosition = 0;
        manager.closeConnection();
        Collections.shuffle(words);
    }

    @Override
    public void refreshWords() {
        getWords();
        currentPosition = -1;
        mView.showNext(nextWord());
    }

    @Override
    public void incCounter(WordModel word) {
        openDatabase();
        manager.incCounter(word);
    }

    private void chooseWords(List<WordModel> source) {
        if (source.size() < quantity || quantity < 1) {
            this.words = source;
        } else {
            Random random = new Random();
            Set<Integer> temp = new HashSet<>();
            while (temp.size() < quantity) { // maybe very long loop
                temp.add(random.nextExpInt(source.size()));
            }
            this.words = new ArrayList<>(quantity);
            for (Integer index: temp) {
                this.words.add(source.get(index));
            }
        }
    }

    private void openDatabase() {
        if (manager == null) {
            manager = new DatabaseManager(mView.getContext());
        }
        manager.openConnection();
    }

}
