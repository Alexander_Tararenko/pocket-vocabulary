package com.gmail.tararenko.pocketvocabulary.domain.backup;

public interface IBackupManager {

    boolean makeBackup();

    boolean restoreFromBackup();

}
