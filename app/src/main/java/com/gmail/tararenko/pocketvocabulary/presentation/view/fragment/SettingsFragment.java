package com.gmail.tararenko.pocketvocabulary.presentation.view.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import com.gmail.tararenko.pocketvocabulary.R;
import com.gmail.tararenko.pocketvocabulary.domain.backup.IBackupManager;
import com.gmail.tararenko.pocketvocabulary.domain.backup.XMLBackupManager;
import com.gmail.tararenko.pocketvocabulary.domain.util.Languages;
import com.gmail.tararenko.pocketvocabulary.presentation.view.component.ThemeMapping;
import com.gmail.tararenko.pocketvocabulary.presentation.view.component.colorpicker.ColorPickerDialog;
import com.gmail.tararenko.pocketvocabulary.presentation.view.component.colorpicker.ColorPickerSwatch;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SettingsFragment extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 101;
    private static final int REQUEST_PERMISSION_SETTING = 201;

    private static Map<Integer, String> themeMap = new HashMap<>();
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        setHasOptionsMenu(true);
        addPreferencesFromResource(R.xml.settings);
        initLanguages();
        PreferenceManager.setDefaultValues(getContext(), R.xml.settings, false);
        for (int i = 0; i < getPreferenceScreen().getPreferenceCount(); i++) {
            pickPreferenceObject(getPreferenceScreen().getPreference(i));
        }
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (key.equals(getString(R.string.preferences_front_key))) {
            String value = sharedPreferences.getString(key, "Word");
            pref.setSummary(value);
        }
        if (key.equals(getString(R.string.preferences_back_key))) {
            String value = sharedPreferences.getString(key, "Translation");
            pref.setSummary(value);
        }
        if (key.equals(getString(R.string.preferences_quantity_key))) {
            String value = sharedPreferences.getString(key, "10");
            pref.setSummary(value);
        }
        if (key.equals(getString(R.string.preferences_translation_from_key))) {
            String value = sharedPreferences.getString(key, "English");
            pref.setSummary(value);
        }
        if (key.equals(getString(R.string.preferences_translation_to_key))) {
            String value = sharedPreferences.getString(key, "Spanish");
            pref.setSummary(value);
        }
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference.getKey().equals(getString(R.string.preferences_backup_key))) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showPermissionDeniedSnackBar();
                } else {
                    this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                }
            } else {
                showBackupDialog();
            }
        }
        if (preference.getKey().equals(getString(R.string.preferences_feedback_key))) {
            createFeedbackIntent();
        }
        if (preference.getKey().equals(getString(R.string.preferences_ui_color_key))) {
            showUIColorPickerDialog();
        }
        if (preference.getKey().equals(getString(R.string.preferences_licenses_key))) {
            showLicenses();
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_settings);
        }
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showBackupDialog();
                } else {
                    showPermissionDeniedSnackBar();
                }
            }
        }
    }

    public Context getContext() {
        return this.getActivity();
    }

    private void pickPreferenceObject(Preference p) {
        if (p instanceof PreferenceCategory) {
            PreferenceCategory cat = (PreferenceCategory) p;
            for (int i = 0; i < cat.getPreferenceCount(); i++) {
                pickPreferenceObject(cat.getPreference(i));
            }
        } else {
            initSummary(p);
        }
    }

    private void initSummary(Preference p) {
        if (p instanceof EditTextPreference) {
            EditTextPreference editTextPref = (EditTextPreference) p;
            p.setSummary(editTextPref.getText());
        }
        if (p instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) p;
            p.setSummary(listPreference.getValue());
        }
    }

    private void showUIColorPickerDialog() {
        if (themeMap.size() == 0) {
            initThemeColors();
        }
        int[] colors = IntegerArrayToPrimitives(themeMap.keySet().toArray(new Integer[themeMap.size()]));
        final int checkedColor = sharedPreferences.getInt(ThemeMapping.THEME_COLOR_KEY, -10453621); // default bluegray color
        ColorPickerDialog colorPickerDialog = ColorPickerDialog.newInstance(
                R.string.color_picker_default_title,
                colors,
                checkedColor,
                5,
                ColorPickerDialog.SIZE_SMALL
        );
        colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                sharedPreferences.edit()
                        .putInt(ThemeMapping.THEME_COLOR_KEY, color)
                        .putString(ThemeMapping.THEME_NAME_KEY, themeMap.get(color))
                        .apply();
                getActivity().recreate();
            }
        });
        colorPickerDialog.show(getFragmentManager(), "useless tag");
    }

    private void createFeedbackIntent() {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.setType("text/email");
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.email)});
        email.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " " + getString(R.string.settings_feedback));
        startActivity(Intent.createChooser(email, getString(R.string.chooser_send_feedback)));
    }

    private void showBackupDialog() {
        String lastDate = sharedPreferences.getString(getString(R.string.preferences_last_backup_key), "");
        String message = getString(R.string.dialog_backup_message);
        if (!lastDate.isEmpty()) {
            message += "\n" + getString(R.string.last_backup_message) + lastDate;
        } else {
            message += "\n" + getString(R.string.no_backup_message);
        }
        createBackupDialog(message);
    }

    private void createBackupDialog(String message) {
        final IBackupManager backupManager = new XMLBackupManager(getContext());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message)
                .setTitle(R.string.backup_dialog_title)
                .setPositiveButton(R.string.backup, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        makeBackup(backupManager);
                    }
                })
                .setNegativeButton(R.string.restore, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restoreFromBackup(backupManager);
                    }
                })
                .setNeutralButton(R.string.cancel, null).show();
    }

    private void showLicenses() {
        WebView view = (WebView) LayoutInflater.from(getContext()).inflate(R.layout.dialog_licenses, null);
        view.loadUrl("file:///android_asset/open_source_licenses.html");
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.open_source_licenses_title))
                .setView(view)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    private void restoreFromBackup(IBackupManager backupManager) {
        if (backupManager.restoreFromBackup()) {
            showSnackBar(getString(R.string.toast_restore_success));
        } else {
            showSnackBar(getString(R.string.toast_restore_fail));
        }
    }

    private void makeBackup(IBackupManager backupManager) {
        if (backupManager.makeBackup()) {
            DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getContext());
            sharedPreferences.edit()
                    .putString(getString(R.string.preferences_last_backup_key),
                            dateFormat.format(Calendar.getInstance().getTime()))
                    .apply();
            showSnackBar(getString(R.string.toast_backup_success));
        } else {
            showSnackBar(getString(R.string.toast_backup_fail));
        }
    }

    private void initLanguages() {
        ListPreference from = (ListPreference) findPreference(getString(R.string.preferences_translation_from_key));
        ListPreference to = (ListPreference) findPreference(getString(R.string.preferences_translation_to_key));
        String[] languages = Languages.getLanguages();
        Arrays.sort(languages);
        from.setEntries(languages);
        from.setEntryValues(languages);
        to.setEntries(languages);
        to.setEntryValues(languages);
    }

    private void initThemeColors() {
        themeMap.put(getResources().getColor(R.color.bluegrayPrimaryColor), ThemeMapping.BLUEGRAY_THEME);
        themeMap.put(getResources().getColor(R.color.redPrimaryColor), ThemeMapping.RED_THEME);
    }

    private int[] IntegerArrayToPrimitives(Integer[] array) {
        int[] result = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i];
        }
        return result;
    }

    private void showPermissionDeniedSnackBar() {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                R.string.permission_denied_message,
                Snackbar.LENGTH_LONG)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    }
                })
                .show();
    }

    private void showSnackBar(String text) {
        Snackbar.make(
                getActivity().findViewById(android.R.id.content),
                text,
                Snackbar.LENGTH_LONG)
                .show();
    }

    private boolean checkExternalWritePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int check = getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return check == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

}
