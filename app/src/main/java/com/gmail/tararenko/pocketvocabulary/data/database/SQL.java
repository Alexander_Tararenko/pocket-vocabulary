package com.gmail.tararenko.pocketvocabulary.data.database;

import com.gmail.tararenko.pocketvocabulary.data.database.table.WordsTable;

public interface SQL {

    String TEXT_TYPE = " TEXT";
    String INTEGER_TYPE = " INTEGER";
    String INTEGER_PRIMARY_KEY_AUTOINCREMENT = " INTEGER PRIMARY KEY AUTOINCREMENT";
    String DEFAULT = " DEFAULT";
    String CURRENT_TIMESTAMP = " CURRENT_TIMESTAMP";
    String COMMA_SEP = ", ";

    String CREATE_TABLE_WORDS = "CREATE TABLE "
            + WordsTable.TABLE_NAME + " ("
            + WordsTable.COLUMN_WORD_ID + INTEGER_PRIMARY_KEY_AUTOINCREMENT + COMMA_SEP
            + WordsTable.COLUMN_WORD + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_TYPE + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_DEFINITION + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_TRANSLATION + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_EXAMPLE + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_EXAMPLE_TRANSLATION + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_SYNONYM + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_COUNTER + INTEGER_TYPE + DEFAULT + "0" + COMMA_SEP
            + WordsTable.COLUMN_NAME_NULLABLE + TEXT_TYPE + COMMA_SEP
            + WordsTable.COLUMN_WORD_CREATION_DATE + INTEGER_TYPE + DEFAULT + CURRENT_TIMESTAMP
            + ")";

    String DELETE_TABLE_WORDS = "DROP TABLE IF EXISTS "
            + WordsTable.TABLE_NAME;

}
