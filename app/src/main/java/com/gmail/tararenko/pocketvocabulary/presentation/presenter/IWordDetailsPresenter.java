package com.gmail.tararenko.pocketvocabulary.presentation.presenter;

import com.gmail.tararenko.pocketvocabulary.presentation.model.WordModel;

public interface IWordDetailsPresenter {

    WordModel getWord(long wordId);

    void deleteWord(long wordId);

    void onOptionsItemSelected(int id);

}
