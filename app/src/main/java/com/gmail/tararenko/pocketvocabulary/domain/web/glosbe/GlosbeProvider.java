package com.gmail.tararenko.pocketvocabulary.domain.web.glosbe;

import com.gmail.tararenko.pocketvocabulary.domain.web.HttpRetriever;
import com.gmail.tararenko.pocketvocabulary.presentation.model.SearchResultModel;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.unbescape.html.HtmlEscape;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class GlosbeProvider {

    private String phrase;
    private String from;
    private String to;
    private boolean isParsed;
    private List<SearchResultModel> results;
    private List<String> examples;
    private List<String> exampleTranslations;

    public GlosbeProvider(String phrase, String from, String to) {
        this.phrase = phrase.toLowerCase().trim().replaceAll(" ", "%20");
        this.from = from;
        this.to = to;
        isParsed = false;
    }

    private static String generateTranslationURL(String phrase, String from, String to) {
        return "https://glosbe.com/gapi/translate?format=json&from=" + from + "&dest=" + to + "&phrase=" + phrase;
    }

    private static String generateExampleURL(String phrase, String from, String to) {
        return "https://glosbe.com/gapi/tm?format=json&from=" + from + "&dest=" + to + "&phrase=" + phrase;
    }

    public void parse() throws IOException {
        isParsed = true;
        String response = HttpRetriever.retrieve(generateTranslationURL(phrase, from, to));
        parseTranslations(response);
//        response = HttpRetriever.retrieve(generateExampleURL(phrase, from, to));
//        parseExamples(response);
    }

    private void parseTranslations(String json) throws IOException {
        results = new LinkedList<>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);
        JsonNode tuc = root.path("tuc");
        Iterator<JsonNode> iterator = tuc.getElements();
        String tempTranslation;
        String tempDefinition;
        JsonNode currentNode;
        while (iterator.hasNext()) {
            currentNode = iterator.next();
            JsonNode phrase = currentNode.findValue("phrase");
            tempTranslation = findValue(phrase, "text");
            List<JsonNode> meanings = currentNode.findValues("meanings");
            for (JsonNode node : meanings) {
                tempDefinition = findValue(node, "text");
                if (tempDefinition != null) {
                    results.add(new SearchResultModel(tempTranslation, tempDefinition));
                }
            }
        }
    }

    private void parseExamples(String json) throws IOException {
        Set<String> examples = new HashSet<>();
        Set<String> exampleTranslations = new HashSet<>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);
        JsonNode ex = root.path("examples");
        Iterator<JsonNode> iterator = ex.getElements();
        String tempString;
        JsonNode currentNode;
        while (iterator.hasNext()) {
            currentNode = iterator.next();
            tempString = findValue(currentNode, "first");
            if (tempString != null) {
                examples.add(tempString);
            }
            tempString = findValue(currentNode, "second");
            if (tempString != null) {
                exampleTranslations.add(tempString);
            }
        }
        this.examples = new ArrayList<>(examples);
        this.exampleTranslations = new ArrayList<>(exampleTranslations);
    }

    private String findValue(JsonNode node, String value) {
        if (node != null) {
            JsonNode text = node.findValue(value);
            if (text != null && !text.getTextValue().isEmpty()) {
                return HtmlEscape.unescapeHtml(text.getTextValue())
                        .replaceAll("\\<.*?>", "").replaceAll("\\{\\{.*", "");
            }
        }
        return null;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        isParsed = false;
        this.phrase = phrase;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        isParsed = false;
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        isParsed = false;
        this.to = to;
    }

    public boolean isParsed() {
        return isParsed;
    }

    public List<SearchResultModel> getResults() {
        return results;
    }

    public List<String> getExamples() {
        return examples;
    }

    public List<String> getExampleTranslations() {
        return exampleTranslations;
    }
}
